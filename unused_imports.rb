SYMBOLS_TO_IGNORE = ["React"]

errors = []
Dir.glob("app/javascript/**/*.{jsx,js}").each do |file|
  filename = file.split("/").last

  imported_symbols = []
  File.open(file, "r") do |f|

    open_bracket = false

    f.each_line do |raw_line|
      line = raw_line.strip

      if !open_bracket

        if line.start_with? "import "
          if line.include? "from "
            scan1 = line.scan(/import (.*) from (.*)/).flatten.first
            imported_symbols += scan1.gsub("{", "").gsub("}", "").split(",")
          elsif line.start_with? "import {"
            open_bracket = true
          else
            imported_part = line.split(" ").last
            if imported_part.include?("'") || imported_part.include?("\"")
              # it's just a literal like a css import
            else
              errors << "Sorry, I don't understand \e[33m#{line}\e[0m in \e[33m#{filename}\e[0m"
            end
          end
        end

      else

        if line.include? "}"
          left, right = line.split("}")

          imported_symbols += left.split(",")

          if right
            # I don't really use this syntax so whatever
            # it's the }, S from
          end

          open_bracket = false
        else
          imported_symbols += line.split(",")
        end

      end

    end
  end

  file_string = File.read(file)

  imported_symbols.each do |symbol|
    symbol = symbol.split(" as ").last.strip
    next if SYMBOLS_TO_IGNORE.include? symbol

    if file_string.gsub(/\b#{symbol}\b/).count == 1
      puts "Unused symbol \e[1m\e[32m#{symbol}\e[0m\e[22m in \e[1m\e[32m#{filename}\e[0m\e[22m"
    end
  end

end

puts errors.join("\n") if errors.size > 0